package com.infotec.ses;

public class UsuarioMail {
	
	int idUsuario;
	String mail;
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	@Override
	public String toString() {
		return "UsuarioMail [idUsuario=" + idUsuario + ", mail=" + mail + "]";
	}
	
	

}
