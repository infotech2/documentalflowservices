package com.infotec.registro;

public class MailProperties {

	private String server;
	private String port;
	private String usrOrigen;
	private String urlUsrMail;
	private String passwd;
	private String copiaObligada;
	
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getUsrOrigen() {
		return usrOrigen;
	}
	public void setUsrOrigen(String usrOrigen) {
		this.usrOrigen = usrOrigen;
	}
	public String getUrlUsrMail() {
		return urlUsrMail;
	}
	public void setUrlUsrMail(String urlUsrMail) {
		this.urlUsrMail = urlUsrMail;
	}
	public String getCopiaObligada() {
		return copiaObligada;
	}
	public void setCopiaObligada(String copiaObligada) {
		this.copiaObligada = copiaObligada;
	}
	@Override
	public String toString() {
		return "MailProperties [server=" + server + ", port=" + port + ", usrOrigen=" + usrOrigen + ", urlUsrMail="
				+ urlUsrMail + ", passwd=" + passwd + ", copiaObligada=" + copiaObligada + "]";
	}
	
}
