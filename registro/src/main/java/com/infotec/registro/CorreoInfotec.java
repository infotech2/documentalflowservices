package com.infotec.registro;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class CorreoInfotec {
	
	private boolean inicio;	
	private int idUsuario;
	private int idUsuarioCopia;
	
	public int getIdUsuarioCopia() {
		return idUsuarioCopia;
	}

	public void setIdUsuarioCopia(int idUsuarioCopia) {
		this.idUsuarioCopia = idUsuarioCopia;
	}

	public boolean isInicio() {
		return inicio;
	}

	public void setInicio(boolean inicio) {
		this.inicio = inicio;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void notifica(String asuntoATratar, String folio) {
		
		String asunto="";
		String texto="";
		if ( isInicio() == true) {
		     asunto="Asignación de atencion en el control documental";
		     texto="Se asignó nuevo documento en el control documental \n Asunto :"+asuntoATratar+"\n Folio:"+folio+"\n Favor entrar a la aplicación para dar la debida atención.";
		}
		else {
		     asunto="Notificación de Termino de atencion en el control documental";
		     texto="Se concluyó atención en el control documental \n Asunto :"+asuntoATratar+"\n Folio:"+folio+"\n Puede consultarse en la aplicación.";			
		}
		String passwd="";
		Propiedades prop = new Propiedades();
		MailProperties mp= prop.getProperties();
		String url;
		String urlCopy;
		url=mp.getUrlUsrMail()+Integer.toString(getIdUsuario());
		urlCopy=mp.getUrlUsrMail()+Integer.toString(getIdUsuarioCopia());
		System.out.println(url);
		System.out.println(urlCopy);
		System.out.println(mp);
		if (mp.getPasswd().equals("no")==false) { // Si no tiene explisitamente no con minusculas, lee el password.
			passwd=mp.getPasswd();
		}
		UsuarioMail um = new UsuarioMail();
		UsuarioMail umCopy = new UsuarioMail();
		Client client = ClientBuilder.newClient();		
		String message = client
		         .target(url)
		         .request(MediaType.APPLICATION_JSON)
		         .get(String.class);
		String messageCopy = client
		         .target(urlCopy)
		         .request(MediaType.APPLICATION_JSON)
		         .get(String.class);
		ObjectMapper om = new ObjectMapper();
		ObjectMapper omCopy = new ObjectMapper(); 
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.host", mp.getServer());
		properties.setProperty("mail.smtp.starttls.enable", "true");
	    properties.setProperty("mail.smtp.port", mp.getPort());
		properties.setProperty("mail.smtp.auth", "true");
		try {
			    um = om.readValue(message, new TypeReference<UsuarioMail>(){});
			    umCopy = omCopy.readValue(messageCopy, new TypeReference<UsuarioMail>(){});
		 	} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
 				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
	    System.out.println(um);
	    System.out.println(umCopy);
	    Session session = Session.getDefaultInstance(properties);
        MimeMessage mail = new MimeMessage(session);
        
        try {
        	mail.setFrom(new InternetAddress(mp.getUsrOrigen()) );
        	mail.addRecipient(Message.RecipientType.TO, new InternetAddress(um.getMail()) );
        	mail.addRecipient(Message.RecipientType.CC, new InternetAddress(umCopy.getMail()) );
        	if (mp.getCopiaObligada().contains("No")==false) {
        		mail.addRecipient(Message.RecipientType.CC, new InternetAddress(mp.getCopiaObligada()) );
        	}
        	mail.setSubject(asunto);
        	mail.setText(texto);
        	Transport transporte = session.getTransport("smtp");
            transporte.connect(mp.getUsrOrigen(), passwd);
        	//transporte.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
        	transporte.sendMessage(mail, mail.getAllRecipients());
        	transporte.close();
        	System.out.println("Mensaje enviado a "+um.getMail() + "con copia para "+umCopy.getMail());
        	
        }catch (AddressException ae) {
        	System.out.println(ae);
        }catch (MessagingException ex){
        	System.out.println(ex);
        }
        return;
		
	}//Fin de clase
	
}
