package com.infotec.registro;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("catalogos")

public class CatPrioridadResource {
	
	CatPrioridadRepo repo = new CatPrioridadRepo();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("prioridades")
	public List<CatPrioridad> getPrioridades() {
		System.out.println("Busqueda al catalogo de prioridades de documentos!");	
		return repo.getPrioridades();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("prioridad/{id_prioridad}")
	public CatPrioridad getPrio(@PathParam("id_prioridad") int id_prioridad) {
		System.out.println("Busqueda al catalogo de prioridades documento con id"+Integer.toString(id_prioridad));	
		return repo.getPrio(id_prioridad);
	}
	

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("semaforo/{id_prioridad}/{idevento}")
	public String getSemaforo(@PathParam("id_prioridad") int id_prioridad, @PathParam("idevento") int idevento) {
		System.out.println("Evalua color correspondiete para el semaforo!");	
		return repo.getSemaforo(id_prioridad,idevento);
	}
	
}
