package com.infotec.registro;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;


public class CatPrioridadRepo {
	
	Connection con = null;
	
	public CatPrioridadRepo(){

		Properties prop= new Properties();
		String propFilename="config.properties";
		String urldb="";
		String usrname="";
		String paswd="";
	
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFilename);
		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		urldb= prop.getProperty("db");
		usrname= prop.getProperty("usr");
		paswd= prop.getProperty("pwd");
 		try {
			Class.forName("org.mariadb.jdbc.Driver");
			con = DriverManager.getConnection(urldb, usrname, paswd);
			System.out.println("Conexion exitosa");
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}  			
	}
	
	
	public List<CatPrioridad> getPrioridades(){
		
		List<CatPrioridad> lista = new ArrayList<>();
		String sql;
	
		sql="Select * from ctldoc.cprioridad";  
		try {
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(sql);
			while (rs.next()) {
				CatPrioridad c = new CatPrioridad();
				c.setId_prioridad(rs.getInt("id_prioridad"));
				c.setPrioridad(rs.getString("prioridad"));
				c.setSla_horas(rs.getInt("sla_horas"));
				lista.add(c);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}
		return lista;	
	}
	
	public CatPrioridad getPrio(int id_prioridad){
		
		String sql;
		CatPrioridad c = new CatPrioridad();
	
		sql="Select * from ctldoc.cprioridad where id_prioridad ="+Integer.toString(id_prioridad);  
		try {
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(sql);
			while (rs.next()) {
				c.setId_prioridad(rs.getInt("id_prioridad"));
				c.setPrioridad(rs.getString("prioridad"));
				c.setSla_horas(rs.getInt("sla_horas"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}
		return c;	
	}
	
	
	public String getSemaforo(int id_prioridad,int idevento){
		
		String sql;
		CatPrioridad c = new CatPrioridad();
		EventoRepo er= new EventoRepo();
		String fecha_ini=er.getFechaIni(1, idevento);
		Date objDate = new Date();
 		String strDateFormat = "yyyy/MM/dd HH:mm:ss";
 		SimpleDateFormat sysdate = new SimpleDateFormat(strDateFormat);
 		Date fechaConvertida = new Date();
		try {
			fechaConvertida = sysdate.parse(fecha_ini);
			System.out.println(objDate);
			System.out.println(fechaConvertida);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
 		Calendar calendar = Calendar.getInstance();
 		calendar.setTime(fechaConvertida);


		int fraccion=0;
			
		sql="Select sla_horas from ctldoc.cprioridad where id_prioridad ="+Integer.toString(id_prioridad);  
		try {
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(sql);
			while (rs.next()) {
				c.setSla_horas(rs.getInt("sla_horas"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}
		fraccion = c.getSla_horas()/4;

	    calendar.add(Calendar.HOUR, fraccion);
	    fechaConvertida = calendar.getTime();
	    if ( objDate.before(fechaConvertida) == true ) {
	    	System.out.println(fechaConvertida);
	    	return "green";
	    }
	    calendar.add(Calendar.HOUR, fraccion);
	    fechaConvertida = calendar.getTime();
	    if ( objDate.before(fechaConvertida) == true ) {
	    	System.out.println(fechaConvertida);
	    	return "yellow";
	    }
	    calendar.add(Calendar.HOUR, fraccion);
	    fechaConvertida = calendar.getTime();
	    if ( objDate.before(fechaConvertida) == true ) {
	    	System.out.println(fechaConvertida);
	    	return "orange";
	    }
	    else {
	    	System.out.println(fechaConvertida);
	    	return "red";
	    }
	}
	

}
