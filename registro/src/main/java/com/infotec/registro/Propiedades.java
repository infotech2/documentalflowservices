package com.infotec.registro;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Propiedades {

	
	public MailProperties getProperties(){
		Properties prop= new Properties();
		String propFilename="config.properties";
		MailProperties mp = new MailProperties();

		
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFilename);
		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		mp.setUrlUsrMail(prop.getProperty("urlusrmail"));
		mp.setPort(prop.getProperty("ptomail"));
		mp.setServer(prop.getProperty("urlcorreo"));
		mp.setUsrOrigen(prop.getProperty("usrorigen"));
		mp.setPasswd(prop.getProperty("passwd"));
		mp.setCopiaObligada(prop.getProperty("copiaobligada"));
		return mp;

	}
}
