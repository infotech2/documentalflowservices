package com.infotec.registro;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("catalogos")


public class CatTipoResource {
	
	CatTipoRepo repo = new CatTipoRepo();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("tipo")
	public List<CatTipo> getTipos() {
		System.out.println("Busqueda al catalogo de tipos de documentos!");	
		return repo.getTipos();
	}
	
}
