package com.infotec.registro;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class Evento {
	private int idProceso;
	private int idEvento;
	private int idUsr;
	private String status;
	private String fechaIni;
	private String fechaFin;
	private String errorMsg;
	private int idPrioridad;
	
	
	public Evento() {
		this.errorMsg="";
	}
	public int getIdProceso() {
		return idProceso;
	}
	public void setIdProceso(int idProceso) {
		this.idProceso = idProceso;
	}
	public int getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(int idEvento) {
		this.idEvento = idEvento;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String estado) {
		status = estado;
	}
	public String getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(String fechaInicial) {
		fechaIni = fechaInicial;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFinal) {
		fechaFin = fechaFinal;
	}
	public int getIdUsr() {
		return idUsr;
	}
	public void setIdUsr(int idUsr) {
		this.idUsr = idUsr;
	}

	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public int getIdPrioridad() {
		return idPrioridad;
	}
	public void setIdPrioridad(int idPrioridad) {
		this.idPrioridad = idPrioridad;
	}
	@Override
	public String toString() {
		return "Evento [idProceso=" + idProceso + ", idEvento=" + idEvento + ", idUsr=" + idUsr + ", status=" + status
				+ ", fechaIni=" + fechaIni + ", fechaFin=" + fechaFin + ", errorMsg=" + errorMsg + ", idPrioridad="
				+ idPrioridad + "]";
	}

}
