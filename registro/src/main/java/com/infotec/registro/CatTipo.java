package com.infotec.registro;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class CatTipo {
	
	private int idTipo;
	private String tipo;
	
	public int getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "CatTipo [idTipo=" + idTipo + ", tipo=" + tipo + "]";
	}
	

}
