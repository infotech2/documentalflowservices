package com.infotec.registro;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement


public class Detalle { 
	
	private int idTarea;
	private int idEvento;
	private int idUsro;
	private int idUsrd;
	private String asunto;
	private String comentarios;
	private String folio;
	private String fecha_ini;
	private String fecha_act;
	private String fecha_fin;
	private String errorMsg;
	private String area;
	private String emisor;
	private String tipo;
	private String fechaRecepcion;
	private String respuesta;
	
	public Detalle() {
		this.errorMsg="";
		this.respuesta="";
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getFechaRecepcion() {
		return fechaRecepcion;
	}
	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		if ( respuesta==null) {
			this.respuesta="";
		}
		else{
		    this.respuesta = respuesta;
		}    
	}
	public int getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(int idTarea) {
		this.idTarea = idTarea;
	}
	public int getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(int idEvento) {
		this.idEvento = idEvento;
	}
	public int getIdUsro() {
		return idUsro;
	}
	public void setIdUsro(int idUsro) {
		this.idUsro = idUsro;
	}
	public int getIdUsrd() {
		return idUsrd;
	}
	public void setIdUsrd(int idUsrd) {
		this.idUsrd = idUsrd;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	public String getFecha_ini() {
		return fecha_ini;
	}
	public void setFecha_ini(String fecha_ini) {
		this.fecha_ini = fecha_ini;
	}
	public String getFecha_act() {
		return fecha_act;
	}
	public void setFecha_act(String fecha_act) {
		this.fecha_act = fecha_act;
	}
	public String getFecha_fin() {
		return fecha_fin;
	}
	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	@Override
	public String toString() {
		return "Detalle [idTarea=" + idTarea + ", idEvento=" + idEvento + ", idUsro=" + idUsro + ", idUsrd=" + idUsrd
				+ ", asunto=" + asunto + ", comentarios=" + comentarios + ", folio=" + folio + ", fecha_ini="
				+ fecha_ini + ", fecha_act=" + fecha_act + ", fecha_fin=" + fecha_fin + ", errorMsg=" + errorMsg
				+ ", area=" + area + ", emisor=" + emisor + ", tipo=" + tipo + ", fechaRecepcion="
				+ fechaRecepcion + ", respuesta=" + respuesta + "]";
	}	


}
