package com.infotec.registro;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement

public class Detalledoc {

    private int idTarea;
    private int idEvento;
    private String fecha_ini;
 	private String asunto;
    private String estadoProceso;
    private String estadoTarea;
    private String folio;
	private String comentarios;
	private String errorMsg;   
	private String area;
	private String emisor;
	private String tipo;
	private String fechaRecepcion;
	private String respuesta;
	private String fecha_fin;
	private String prioridad;
	private String semaforo;
	private int idPrioridad;
	private int idPremuraSemaforo;
    
    private List<InformacionDetalle> documentos = new ArrayList<>();

    public int getIdTarea() {
		return idTarea;
	}
	public void setIdTarea(int idTarea) {
		this.idTarea = idTarea;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public List<InformacionDetalle> getDocumentos() {
		return documentos;
	}
	public void setDocumentos(List<InformacionDetalle> documentos) {
		this.documentos = documentos;
	}
	public String getEstadoProceso() {
		return estadoProceso;
	}
	public void setEstadoProceso(String estadoProceso) {
		this.estadoProceso = estadoProceso;
	}
	public String getEstadoTarea() {
		return estadoTarea;
	}
    public String getFecha_ini() {
		return fecha_ini;
	}
	public void setFecha_ini(String fecha_ini) {
		this.fecha_ini = fecha_ini;
	}
	public void setEstadoTarea(String estadoTarea) {
		this.estadoTarea = estadoTarea;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public int getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(int idEvento) {
		this.idEvento = idEvento;
	}
    public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getFechaRecepcion() {
		return fechaRecepcion;
	}
	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getFecha_fin() {
		return fecha_fin;
	}
	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}
	public String getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getSemaforo() {
		return semaforo;
	}
	public void setSemaforo(String semaforo) {
		this.semaforo = semaforo;
	}
	public int getIdPrioridad() {
		return idPrioridad;
	}
	public void setIdPrioridad(int idPrioridad) {
		this.idPrioridad = idPrioridad;
	}
	public int getIdPremuraSemaforo() {
		return idPremuraSemaforo;
	}
	public void setIdPremuraSemaforo(int idPremuraSemaforo) {
		this.idPremuraSemaforo = idPremuraSemaforo;
	}
	@Override
	public String toString() {
		return "Detalledoc [idTarea=" + idTarea + ", idEvento=" + idEvento + ", fecha_ini=" + fecha_ini + ", asunto="
				+ asunto + ", estadoProceso=" + estadoProceso + ", estadoTarea=" + estadoTarea + ", folio=" + folio
				+ ", comentarios=" + comentarios + ", errorMsg=" + errorMsg + ", area=" + area + ", emisor=" + emisor
				+ ", tipo=" + tipo + ", fechaRecepcion=" + fechaRecepcion + ", respuesta=" + respuesta + ", fecha_fin="
				+ fecha_fin + ", prioridad=" + prioridad + ", semaforo=" + semaforo + ", idPrioridad=" + idPrioridad
				+ ", idPremuraSemaforo=" + idPremuraSemaforo + ", documentos=" + documentos + "]";
	}
	
}
