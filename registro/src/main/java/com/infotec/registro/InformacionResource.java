package com.infotec.registro;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType; 

@Path("docu")

public class InformacionResource {

	InformacionRepo repo = new InformacionRepo();
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("informacion/{idDocumento}")
	public Informacion getInfo(@PathParam("idDocumento") int idDocumento) {
		System.out.println("Ejecuta info documento!");	
		return repo.getInfo(idDocumento);
	}	

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("info/{idEvento}")
	public  List<Informacion>  getInformacion(@PathParam("idEvento") int idEvento) {
		System.out.println("Ejecuta informacion lista!");	
		return repo.getInformacion(idEvento);
	}	

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("infotipo/{idEvento}/{idTipo}")
	public List<Informacion> getInformacionTipo(@PathParam("idEvento") int idEvento, @PathParam("idTipo") int idTipo) {
		System.out.println("Ejectua informacion x tipo!");		
		return repo.getInformacionTipo(idEvento,idTipo);
	}	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("infoname/{nombre}")
	public Informacion getInfoName(@PathParam("nombre") String nombre) {
		System.out.println("Ejecuta info documento por nombre!");	
		return repo.getInfoName(nombre);
	}	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("cinformacion")
	public Informacion createInformacion(Informacion informacion) {
		Informacion auxiliar = new Informacion();
		auxiliar=getInfoName(informacion.getNombre());
		if (auxiliar.getIdDocumento()==0) { //Es un nuevo nombre de documento y se puede dar de alta		
		     repo.create(informacion);
		     System.out.println(informacion);
		     auxiliar=getInfoName(informacion.getNombre());
		     if  (auxiliar.getIdDocumento()==0) {
		    	 informacion.setErrorMsg("Error: No fué posible dar de alta el documento en la base de datos");
		     }
		}
		else {
			informacion.setErrorMsg("Ya existe un documento con el mismo nombre");
		}
		return informacion;
	}
	
	
}
