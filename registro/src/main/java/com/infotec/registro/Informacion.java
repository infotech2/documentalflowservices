package com.infotec.registro;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement


public class Informacion {
	
    private int idEvento;
    private int idDocumento;
    private String documento;
    private String fechaAdd;
    private int idTipo;
    private String errorMsg;
    private String nombre;
    
    public Informacion() {
    	this.errorMsg="";
    }
	
	public int getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(int idEvento) {
		this.idEvento = idEvento;
	}

	public int getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(int idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String doc) {
		documento = doc;
	}

	public String getFechaAdd() {
		return fechaAdd;
	}

	public void setFechaAdd(String fechaAdd) {
		this.fechaAdd = fechaAdd;
	}
	
	public int getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Informacion [idEvento=" + idEvento + ", idDocumento=" + idDocumento + ", documento=" + documento
				+ ", fechaAdd=" + fechaAdd + ", idTipo=" + idTipo + ", errorMsg=" + errorMsg + ", nombre=" + nombre
				+ "]";
	}

	
	
}
