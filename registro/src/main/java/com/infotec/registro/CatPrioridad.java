package com.infotec.registro;

public class CatPrioridad {

	private int id_prioridad;
	private String prioridad;
	private int sla_horas;
	
	public int getId_prioridad() {
		return id_prioridad;
	}

	public void setId_prioridad(int id_prioridad) {
		this.id_prioridad = id_prioridad;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	
	public int getSla_horas() {
		return sla_horas;
	}

	public void setSla_horas(int sla_horas) {
		this.sla_horas = sla_horas;
	}

	@Override
	public String toString() {
		return "CatPrioridad [id_prioridad=" + id_prioridad + ", prioridad=" + prioridad + ", sla_horas=" + sla_horas
				+ "]";
	}
	
}
