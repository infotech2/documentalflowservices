package com.infotec.registro1;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DetalledocRepo {
	
	Connection con = null;
	
	public DetalledocRepo(){

		Properties prop= new Properties();
		String propFilename="config.properties";
		String urldb="";
		String usrname="";
		String paswd="";
	
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFilename);
		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		urldb= prop.getProperty("db");
		usrname= prop.getProperty("usr");
		paswd= prop.getProperty("pwd");
 		try {
			Class.forName("org.mariadb.jdbc.Driver");
			con = DriverManager.getConnection(urldb, usrname, paswd);
			System.out.println("Conexion exitosa");
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}  			
	}


	public String getTipoDocumento(int idTipo) {
        String sql="Select tipo from ctldoc.ctipo where idtipo="+Integer.toString(idTipo);
        String tipo="";
	    try {
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(sql);

			while (rs.next()) {	
	      		tipo=rs.getString("tipo");
			}
			
		 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}
        return tipo;
	}
	
		
	public  List<Detalledoc> getDetalleDoc( int idUsr) {
		String idUsuario=Integer.toString(idUsr);
		List<Detalledoc> detdoc = new ArrayList<>();				
		String tipo="";
		String sql="SELECT  detalle.*, tarea.nomtarea, evento.estado FROM ctldoc.detalle, ctldoc.evento, ctldoc.tarea WHERE detalle.fecha_fin IS NULL AND detalle.idtarea=tarea.idtarea AND detalle.idevento=evento.idevento AND ( detalle.idusro = " + idUsuario + " OR detalle.idusrd = " + idUsuario + " ) order by evento.idevento";
		int idTipo=0;
		int lastTipo=0;
	    try {
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(sql);
			InformacionRepo irepo = new InformacionRepo();
			while (rs.next()) {	
				int idEventoQuery=rs.getInt("idevento");
	      		Detalledoc d = new Detalledoc();
	      		List<Informacion> info = new ArrayList<>();
	    		List<InformacionDetalle> infoDetalle = new ArrayList<>();
	      		d.setIdEvento(idEventoQuery);
	      		d.setAsunto(rs.getString("asunto"));
	      		d.setEstadoProceso(rs.getString("estado"));
	      		d.setEstadoTarea(rs.getString("nomtarea"));
	      		d.setFolio(rs.getString("folio"));
	      		d.setComentarios(rs.getString("comentarios"));
	      		d.setIdTarea (rs.getInt("idtarea"));
	      		info=irepo.getInformacion(idEventoQuery);
	      		System.out.print("idEventoQuery="+idEventoQuery+"info=");
	      		System.out.println(info);
	      		for (int i=0; i<info.size(); i++) {
	      			idTipo = info.get(i).getIdTipo();
	      			if ( idTipo != lastTipo ) {
	      				InformacionDetalle infoDet = new InformacionDetalle();
	      				List<Informacion> infoNueva = new ArrayList<>();
	      			    tipo=getTipoDocumento(idTipo);
		      			infoDet.setIdTipo(idTipo);
		      			infoDet.setNombreTipo(tipo);
      					infoNueva=irepo.getInformacionTipo(idEventoQuery, idTipo);
      					infoDet.setItems(infoNueva);
      					infoDetalle.add(infoDet);
	      			    System.out.println( " idTipo="+ Integer.toString(idTipo) + " tipo=" +tipo + " i=" + Integer.toString(i) );
	      			    lastTipo=idTipo;
	      			}
	      		}
	      		d.setDocumentos(infoDetalle);
	      		detdoc.add(d);
			}
			
		 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}
	    if (detdoc.isEmpty()) {
	    	Detalledoc d = new Detalledoc();
	    	d.setErrorMsg("No hay elementos que cumplan con los criterios de búsqueda");
	    	detdoc.add(d); 
	    }
		return detdoc;	
	}

	public  Detalledoc getOneDetalleDoc( int idUsr, int idEvento) {
				
		String idUsuario=Integer.toString(idUsr);
		String idEve=Integer.toString(idEvento);
		Detalledoc d = new Detalledoc();
		List<InformacionDetalle> infoDetalle = new ArrayList<>();
		List<Informacion> info = new ArrayList<>();
		String tipo="";
		int idTipo=0;
		int lastTipo=0;
		
		String sql="SELECT  detalle.*, tarea.nomtarea, evento.estado FROM ctldoc.detalle, ctldoc.evento, ctldoc.tarea WHERE detalle.fecha_fin IS NULL AND detalle.idtarea=tarea.idtarea AND detalle.idevento=evento.idevento AND ( detalle.idusro = " + idUsuario + " OR detalle.idusrd = " + idUsuario + " ) AND detalle.idevento=" + idEve;
	    try {
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery(sql);
			InformacionRepo irepo = new InformacionRepo();
			while (rs.next()) {	;
				int idEventoQuery=rs.getInt("idevento");
				d.setIdEvento(idEventoQuery);
	      		d.setAsunto(rs.getString("asunto"));
	      		d.setEstadoProceso(rs.getString("estado"));
	      		d.setEstadoTarea(rs.getString("nomtarea"));
	      		d.setFolio(rs.getString("folio"));
	      		d.setComentarios(rs.getString("comentarios"));
	      		d.setIdTarea (rs.getInt("idtarea"));
	      		info=irepo.getInformacion(idEventoQuery);
	      		System.out.println(info);
	      		System.out.println();
	      		for (int i=0; i<info.size(); i++) {
	      			idTipo = info.get(i).getIdTipo();
	      			if ( idTipo != lastTipo ) {
	      				InformacionDetalle infoDet = new InformacionDetalle();
	      				List<Informacion> infoNueva = new ArrayList<>();
	      			    tipo=getTipoDocumento(idTipo);
		      			infoDet.setIdTipo(idTipo);
		      			infoDet.setNombreTipo(tipo);
      					infoNueva=irepo.getInformacionTipo(idEventoQuery, idTipo);
      					infoDet.setItems(infoNueva);
      					infoDetalle.add(infoDet);
	      			    System.out.println( " idTipo="+ Integer.toString(idTipo) + " tipo=" +tipo + " i=" + Integer.toString(i) );
	      			    lastTipo=idTipo;
	      			}
	      		}
	      		System.out.print("infoDetalle = ");
	      		System.out.println(infoDetalle);
	      		d.setDocumentos(infoDetalle);
			}
			
		 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}
	    if (d.getIdTarea()==0) {
	    	d.setErrorMsg("No existe informacón con los criterios de búsqueda seleccionados");
	    }
		return d;	
	}	
	
}
