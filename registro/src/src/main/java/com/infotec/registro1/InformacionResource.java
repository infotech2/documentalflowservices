package com.infotec.registro1;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("docu")

public class InformacionResource {

	InformacionRepo repo = new InformacionRepo();
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("informacion/{idDocumento}")
	public Informacion getInfo(@PathParam("idDocumento") int idDocumento) {
		System.out.println("Ejecuta info documento!");	
		return repo.getInfo(idDocumento);
	}	

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("info/{idEvento}")
	public  List<Informacion>  getInformacion(@PathParam("idEvento") int idEvento) {
		System.out.println("Ejecuta informacion lista!");	
		return repo.getInformacion(idEvento);
	}	

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("infotipo/{idEvento}/{idTipo}")
	public List<Informacion> getInformacionTipo(@PathParam("idEvento") int idEvento, @PathParam("idTipo") int idTipo) {
		System.out.println("Ejectua informacion x tipo!");		
		return repo.getInformacionTipo(idEvento,idTipo);
	}	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("cinformacion")
	public Informacion createInformacion(Informacion informacion) {
		repo.create(informacion);
		System.out.println(informacion);
		return informacion;
	}
	
	
}
