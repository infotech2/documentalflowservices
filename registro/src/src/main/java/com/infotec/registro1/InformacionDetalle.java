package com.infotec.registro1;

import java.util.List;

public class InformacionDetalle {
	private int idTipo;
	private String nombreTipo;
	private List<Informacion> items;
	
	public int getIdTipo() {
		return idTipo;
	}
	public void setIdTipo(int idTipo) {
		this.idTipo = idTipo;
	}
	public String getNombreTipo() {
		return nombreTipo;
	}
	public void setNombreTipo(String nombreTipo) {
		this.nombreTipo = nombreTipo;
	}
	public List<Informacion> getItems() {
		return items;
	}
	public void setItems(List<Informacion> items) {
		this.items = items;
	}
	
	@Override
	public String toString() {
		return "InformacionDetalle [ idTipo= "+idTipo+", nombreTipo= "+nombreTipo+", Items= {"+items+"}]";
	}

}
